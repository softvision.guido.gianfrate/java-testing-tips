package com.softvision.spring_demo.students.service;

import com.softvision.spring_demo.students.dto.DeleteResponseDTO;
import com.softvision.spring_demo.students.vo.StudentVO;
import com.softvision.spring_demo.students.model.entities.Student;
import com.softvision.spring_demo.students.exception.ElementNotFoundException;
import com.softvision.spring_demo.students.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<Student> getAllStudents(String firstname, String lastname, List<Integer> age) {
        List<Student> allStudents = studentRepository.findByFirstnameStartingWithAndLastnameStartingWithAndAgeIn(firstname, lastname, age);
        return allStudents;
    }

    public Student getStudentById(Long id) {
        Optional<Student> studentOptional = studentRepository.findById(id);
        if (!studentOptional.isPresent()) throw new ElementNotFoundException("The Student does not exist");
        Student studentFromDb = studentOptional.get();
        return studentFromDb;
    }

    public Student saveStudent(StudentVO studentVO) {
        Student entity = studentRepository.save(convertStudentVoToEntity(studentVO));
        return entity;
    }

    public DeleteResponseDTO deleteStudent(Long id) {
        getStudentById(id);
        studentRepository.deleteById(id);
        return new DeleteResponseDTO(id, "Deleted");
    }

    private Student convertStudentVoToEntity(StudentVO studentVO) {
        Student student = modelMapper.map(studentVO, Student.class);
        if (studentVO.getId() != null) {
            student.setId(studentVO.getId());
        }
        return student;
    }
}