package com.softvision.spring_demo.students.model.entities;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "Students")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Student {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "age")
    private Integer age;
    @Column(name = "average")
    private Double average;

    public Student(String firstname, String lastname, Integer age, Double average) {
        this.firstname=firstname;
        this.lastname=lastname;
        this.age=age;
        this.average = average;
    }
}
