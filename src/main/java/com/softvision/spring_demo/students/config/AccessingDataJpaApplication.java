package com.softvision.spring_demo.students.config;

import com.softvision.spring_demo.students.model.entities.Student;
import com.softvision.spring_demo.students.repository.StudentRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AccessingDataJpaApplication {

    private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(AccessingDataJpaApplication.class);
    }

    @Bean
    public CommandLineRunner demo(StudentRepository repository) {
        return (args) -> {
            // save a few students
            repository.save(new Student(1L,"Guido", "Gianfrate", 20, 5.5));
            repository.save(new Student("Fernando", "Palacios", 30, 8.2));
            repository.save(new Student("Francisco", "Migliaro", 32, 9.0));
            repository.save(new Student("Emanuel", "Ocampo", 47, 6.7));
            repository.save(new Student("David", "Gomez", 45, 7.2));
            repository.save(new Student("Martin", "Gomez", 32, 2.4));
            repository.save(new Student("Gonzalo", "Roque", 25, 5.7));
            repository.save(new Student("Federico", "Motta", 41, 6.7));
            repository.save(new Student("Nahuel", "Aste", 34, 5.7));
            repository.save(new Student("Federico", "Montaldo", 35, 4.6));
            repository.save(new Student("Sol", "Rascon", 21, 2.3));
            repository.save(new Student("Dylan", "Goldstein", 21, 6.7));
            repository.save(new Student("Franco", "Dipace", 23, 8.9));
            repository.save(new Student("Ricardo", "Perez", 43, 6.0));
            repository.save(new Student("Ricardo", "Fort", 45, 5.6));
            repository.save(new Student("Valenti", "SanJuan", 35, 9.9));
            repository.save(new Student("Julia", "Perez", 47, 10.0));
            repository.save(new Student("Camila", "Perez", 46, 4.5));
            repository.save(new Student("Camila", "Perez", 45, 5.6));
            repository.save(new Student("Camila", "Goycochea", 45, 7.4));
            repository.save(new Student("Julieta", "Godoy", 50, 6.5));
            repository.save(new Student("Alba", "Aguilar", 22, 4.5));
            repository.save(new Student("Nicolas", "Goycochea", 26, 5.6));
            repository.save(new Student("Matias", "Longo", 21, 6.6));
            // fetch all students
            log.info("Student found with findById(1):");
            Student customer = repository.findById(1L).get();
            log.info(customer.toString());
            log.info("Students loaded correctly");
            log.info("---------------------------");


        };
    }


}

