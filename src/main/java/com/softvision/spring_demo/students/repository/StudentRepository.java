package com.softvision.spring_demo.students.repository;

import com.softvision.spring_demo.students.model.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
    List<Student> findByFirstnameStartingWithAndLastnameStartingWithAndAgeIn(String firstname, String lastname, List<Integer> age);
}
