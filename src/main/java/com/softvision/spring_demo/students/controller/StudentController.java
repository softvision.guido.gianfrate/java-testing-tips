package com.softvision.spring_demo.students.controller;

import com.softvision.spring_demo.students.dto.DeleteResponseDTO;
import com.softvision.spring_demo.students.dto.StudentRequest;
import com.softvision.spring_demo.students.model.entities.Student;
import com.softvision.spring_demo.students.vo.StudentVO;
import com.softvision.spring_demo.students.helpers.PossibleAgesUtil;
import com.softvision.spring_demo.students.helpers.StudentRequestValidator;
import com.softvision.spring_demo.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequestMapping("/api")
@Controller
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    StudentRequestValidator studentRequestValidator;

    @GetMapping("/students")
    public ResponseEntity<List<Student>> getAllStudents(@RequestParam Optional<String> firstname,
                                                          @RequestParam Optional<String> lastname,
                                                          @RequestParam(required = false) ArrayList<Integer> age) {

        PossibleAgesUtil possibleAgesUtil = new PossibleAgesUtil();
        ArrayList<Integer> possibleAges = possibleAgesUtil.getSearchedAges(age, 3, 99);

        List<Student> allStudents = studentService.getAllStudents(firstname.orElse(""),
                lastname.orElse(""),
                possibleAges);

        return new ResponseEntity<>(allStudents, HttpStatus.OK);
    }

    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable Long id) {
        Student student = studentService.getStudentById(id);
        return new ResponseEntity<Student>(student, HttpStatus.OK);
    }

    @PostMapping("/students")
    public ResponseEntity<Student> addNewStudent(@RequestBody StudentRequest student) {

        StudentVO studentVO = new StudentVO(student);
        Student studentEntity = studentService.saveStudent(studentVO);

        return new ResponseEntity<>(studentEntity, HttpStatus.CREATED);
    }

    @PutMapping("/students/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody StudentRequest student) {

        studentService.getStudentById(id);
        student.setId(id);
        StudentVO studentVO = new StudentVO(student);
        Student studentEntity = studentService.saveStudent(studentVO);
        return new ResponseEntity<>(studentEntity, HttpStatus.OK);
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<DeleteResponseDTO> deleteStudent(@PathVariable Long id) {
        studentService.getStudentById(id);
        return new ResponseEntity<>(studentService.deleteStudent(id), HttpStatus.OK);
    }
}