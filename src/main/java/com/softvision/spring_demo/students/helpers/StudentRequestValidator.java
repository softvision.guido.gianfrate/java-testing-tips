package com.softvision.spring_demo.students.helpers;

import com.softvision.spring_demo.students.dto.StudentRequest;
import com.softvision.spring_demo.students.exception.StudentRequestValidatorException;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class StudentRequestValidator {

    private static final Integer MAX_AGE = 110;
    private static final Integer MIN_AGE = 0;
    private static final Double MAX_AVERAGE = 10.0;
    private static final Double MIN_AVERAGE = 0.0;

    public static final String INVALID_AGE_MSG = "The age is not valid";
    public static final String INVALID_NAME_MSG = "The Firstname or LastName is null or empty";
    public static final String INVALID_AVERAGE_MSG = "The average is not valid";

    public void validateStudentRequest(StudentRequest studentRequest){
        checkNotNullName(studentRequest.getFirstname());
        checkNotNullName(studentRequest.getLastname());
        checkAge(studentRequest.getAge());
        checkAverage(studentRequest.getAverage());
    }

    private void checkNotNullName(String name){
        if(StringUtils.isBlank(name)){
            throw new StudentRequestValidatorException(INVALID_NAME_MSG);
        }
    }

    public void checkAge(Integer age){
        if(age > MAX_AGE || age < MIN_AGE){
            throw new StudentRequestValidatorException(INVALID_AGE_MSG);
        }
    }

    public void checkAverage(Double average){
        if(average > MAX_AVERAGE || average < MIN_AVERAGE){
            throw new StudentRequestValidatorException(INVALID_AVERAGE_MSG);
        }
    }
}