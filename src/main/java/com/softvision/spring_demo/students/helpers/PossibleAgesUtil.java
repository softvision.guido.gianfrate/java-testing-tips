package com.softvision.spring_demo.students.helpers;

import java.util.ArrayList;

public class PossibleAgesUtil {

    public ArrayList<Integer> getSearchedAges(ArrayList<Integer> entryAges, int ageFrom, int ageTo) {

        //if the list has values return these values
        if (entryAges != null) return entryAges;

        //if the list not has values, the list has the numbers from -ageFrom- to -ageTo-.
        ArrayList<Integer> possibleAges = new ArrayList<>();
        for (int i = ageFrom; i < ageTo; i++) {
            possibleAges.add(i);
        }
        return possibleAges;
    }
}
