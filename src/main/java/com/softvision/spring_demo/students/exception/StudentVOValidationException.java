package com.softvision.spring_demo.students.exception;

public class StudentVOValidationException extends RuntimeException{

    String logMsg;

    public StudentVOValidationException(String message,Throwable cause){
        super(message,cause);
    }

    public StudentVOValidationException(String message){
        super(message);
    }

    public StudentVOValidationException(Throwable cause){
        super(cause);
    }
}
