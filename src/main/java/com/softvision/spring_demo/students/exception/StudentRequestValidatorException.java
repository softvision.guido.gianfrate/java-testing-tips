package com.softvision.spring_demo.students.exception;

public class StudentRequestValidatorException extends RuntimeException{

    String logMsg;

    public StudentRequestValidatorException(String message, Throwable cause){
        super(message,cause);
    }

    public StudentRequestValidatorException(String message){
        super(message);
    }

    public StudentRequestValidatorException(Throwable cause){
        super(cause);
    }
}
