package com.softvision.spring_demo.students.vo;

import com.softvision.spring_demo.students.dto.StudentRequest;
import com.softvision.spring_demo.students.exception.StudentVOValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.junit.platform.commons.util.StringUtils;

@Value
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class StudentVO {

    private static final Integer MAX_AGE = 150;
    private static final Integer MIN_AGE = 0;
    private static final Double MAX_AVERAGE = 10.0;
    private static final Double MIN_AVERAGE = 0.0;

    private static final String INVALID_AGE_MSG = "The age is not valid";
    private static final String INVALID_NAME_MSG = "The Firstname or LastName is null or empty";
    private static final String INVALID_AVERAGE_MSG = "The average is not valid";

    @ApiModelProperty(notes = "Student ID", hidden = true)
    private Long id;
    @ApiModelProperty(notes = "Students's first name")
    private String firstname;
    @ApiModelProperty(notes = "Students's last name")
    private String lastname;
    @ApiModelProperty(notes = "Students's age")
    private Integer age;
    @ApiModelProperty(notes = "Students's average")
    private Double average;

    public StudentVO(StudentRequest student){

        this.id = student.getId();
        this.firstname = getAndCheckNotNullName(student.getFirstname());
        this.lastname = getAndCheckNotNullName(student.getLastname());
        this.age = getAndCheckAge(student.getAge());
        this.average = getAndCheckAverage(student.getAverage());
    }

    ///////////////////
    //Private Methods//
    ///////////////////

    private String getAndCheckNotNullName(String name){
        if(StringUtils.isBlank(name)){
            throw new StudentVOValidationException(INVALID_NAME_MSG);
        }
        return name;
    }

    private Integer getAndCheckAge(Integer age){
        if(age > MAX_AGE || age < MIN_AGE){
            throw new StudentVOValidationException(INVALID_AGE_MSG);
        }
        return age;
    }

    private Double getAndCheckAverage(Double average){
        if(average > MAX_AVERAGE || average < MIN_AVERAGE){
            throw new StudentVOValidationException(INVALID_AVERAGE_MSG);
        }
        return average;
    }
}