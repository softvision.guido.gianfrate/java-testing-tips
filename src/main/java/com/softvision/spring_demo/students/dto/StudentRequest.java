package com.softvision.spring_demo.students.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentRequest {
    @ApiModelProperty(notes = "Student ID", hidden = true)
    private Long id;
    @ApiModelProperty(notes = "Students's first name")
    private String firstname;
    @ApiModelProperty(notes = "Students's last name")
    private String lastname;
    @ApiModelProperty(notes = "Students's age")
    private Integer age;
    @ApiModelProperty(notes = "Students's average")
    private Double average;
}
