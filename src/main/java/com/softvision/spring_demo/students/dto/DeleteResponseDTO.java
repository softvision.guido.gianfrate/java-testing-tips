package com.softvision.spring_demo.students.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DeleteResponseDTO {

    private Long id;
    private String response;

}
