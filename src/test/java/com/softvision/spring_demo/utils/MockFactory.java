package com.softvision.spring_demo.utils;

import com.softvision.spring_demo.students.dto.StudentRequest;
import com.softvision.spring_demo.students.model.entities.Student;
import com.softvision.spring_demo.students.vo.StudentVO;

import java.util.ArrayList;
import java.util.List;

public class MockFactory {

    public static final Integer MAX_AGE = 110;
    public static final Integer MIN_AGE = 0;
    public static final Double MAX_AVERAGE = 10.0;
    public static final Double MIN_AVERAGE = 0.0;

    private static StudentRequest.StudentRequestBuilder getValidStudentRequestBuilder(){
        return StudentRequest.builder()
                .firstname("Pedro")
                .lastname("Alvarez")
                .age(32)
                .average(5.5);
    }

    public static StudentRequest getValidStudentRequest(){
        return getValidStudentRequestBuilder()
                .build();
    }

    public static StudentRequest getInvalidStudentRequest_moreAgeThatPossible(){
        return getValidStudentRequestBuilder()
                .age(MAX_AGE + 1)
                .build();
    }

    public static StudentRequest getInvalidStudentRequest_minorAgeThatPossible(){
        return getValidStudentRequestBuilder()
                .age(MIN_AGE - 1)
                .build();
    }

    public static StudentRequest getInvalidStudentRequest_moreAverageThatPossible(){
        return StudentRequest.builder()
                .firstname("Pedro")
                .lastname("Alvarez")
                .age(32)
                .average(MAX_AVERAGE + 1)
                .build();
    }

    public static StudentRequest getInvalidStudentRequest_minorAverageThatPossible(){
        return StudentRequest.builder()
                .firstname("Pedro")
                .lastname("Alvarez")
                .age(32)
                .average(MIN_AVERAGE - 1)
                .build();
    }

    public static StudentRequest getInvalidStudentRequest_nullName(){
        return StudentRequest.builder()
                .firstname(null)
                .lastname("Alvarez")
                .age(32)
                .average(5.5)
                .build();
    }


    public static StudentRequest getInvalidStudentRequest_nullLastName(){
        return StudentRequest.builder()
                .firstname("Pedro")
                .lastname(null)
                .age(32)
                .average(5.5)
                .build();
    }

    public static StudentVO getValidStudentVO(){
        return StudentVO.builder()
                .firstname("Pedro")
                .lastname("Alvarez")
                .id(1L)
                .age(32)
                .average(5.5)
                .build();
    }

    public static StudentVO getOtherValidStudentVO(){

        return StudentVO.builder()
                .firstname("Guido")
                .lastname("Gianfrate")
                .id(2L)
                .age(20)
                .average(7.8)
                .build();

    }

    public static Student getValidStudent(){
        return Student.builder()
                .firstname("Pedro")
                .lastname("Alvarez")
                .id(1L)
                .age(32)
                .average(5.5)
                .build();
    }

    public static Student getOtherValidStudent(){

        return Student.builder()
                .firstname("Guido")
                .lastname("Gianfrate")
                .id(2L)
                .age(20)
                .average(7.8)
                .build();

    }

    public static List<StudentVO> getValidStudentsVO(){

        ArrayList<StudentVO> students = new ArrayList<>();
        students.add(getValidStudentVO());
        students.add(getOtherValidStudentVO());
        return students;
    }

    public static List<Student> getValidStudents(){

        ArrayList<Student> students = new ArrayList<>();
        students.add(getValidStudent());
        students.add(getOtherValidStudent());
        return students;
    }


}
