package com.softvision.spring_demo.integration;

import com.softvision.spring_demo.students.StudentsApplication;
import com.softvision.spring_demo.students.dto.DeleteResponseDTO;
import com.softvision.spring_demo.students.dto.StudentRequest;
import com.softvision.spring_demo.students.vo.StudentVO;
import com.softvision.spring_demo.students.exception.ErrorResponse;
import com.softvision.spring_demo.students.helpers.ConnectionToApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

@SpringBootTest(classes = StudentsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class IntegrationTest {
    private static final String LOCALHOST = "http://localhost:";
    private static final String CONTEXT_PATH = "/api";
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate testRestTemplate;
    private ConnectionToApi connectionToApi;

    @Before
    public void before() {
        connectionToApi = new ConnectionToApi();
    }

    @Test
    public void getAllStudents(){
        final String url = LOCALHOST + port + CONTEXT_PATH;
        final String name = "Guido";
        final String lastname = "Gianfrate";
        final int age = 20;
        final long id = 1L;
        ResponseEntity<List<StudentVO>> httpResponse = testRestTemplate.exchange(url+"/students", HttpMethod.GET, null, new ParameterizedTypeReference<List<StudentVO>>() {});
        List<StudentVO> responseListStudents = httpResponse.getBody();

        assertThat(responseListStudents).isNotEmpty();

        StudentVO firstStudent = responseListStudents.get(0);

        assertThat(firstStudent.getFirstname()).isEqualTo(name);
        assertThat(firstStudent.getLastname()).isEqualTo(lastname);
        assertThat(firstStudent.getAge()).isEqualTo(age);
        assertThat(firstStudent.getId()).isEqualTo(id);
        assertThat(httpResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void getStudentById(){
        final String url = LOCALHOST + port + CONTEXT_PATH;
        ResponseEntity<StudentVO> httpResponse = testRestTemplate.getForEntity(url+"/students/8", StudentVO.class);

        StudentVO studentVO = httpResponse.getBody();

        assertThat(studentVO.getAge()).isEqualTo(41);
        assertThat(studentVO.getId()).isEqualTo(8);
        assertThat(studentVO.getFirstname()).isEqualTo("Federico");
        assertThat(studentVO.getLastname()).isEqualTo("Motta");
        assertThat(httpResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void getStudentByNotExistentId() {
        final String url = LOCALHOST + port + CONTEXT_PATH;
        boolean isThrowingException = false;
        try {
            connectionToApi.connect(url + "/students/2000", HttpMethod.GET, StudentVO.class, null);
        }catch(HttpClientErrorException.NotFound ex){
            isThrowingException = true;
        }
        assertTrue(isThrowingException);
    }

    @Test
    public void postAndDeleteStudent()  {
        final String url = LOCALHOST + port + CONTEXT_PATH;

        StudentRequest postStudent = new StudentRequest();
        postStudent.setFirstname("Chuck");
        postStudent.setLastname("Norris");
        postStudent.setAge(80);
        postStudent.setAverage(5.5);
        ResponseEntity<StudentVO> httpResponsePost = testRestTemplate.postForEntity(url+"/students",postStudent,StudentVO.class);

        StudentVO newStudent = httpResponsePost.getBody();

        assertTrue(newStudent.getAge().equals(postStudent.getAge()));
        assertTrue(newStudent.getFirstname().equals(postStudent.getFirstname()));
        assertTrue(newStudent.getLastname().equals(postStudent.getLastname()));
        testRestTemplate.delete(url+"/students/"+newStudent.getId(), String.class);

        ResponseEntity<ErrorResponse> errorBody = testRestTemplate.getForEntity(url+"/students/"+newStudent.getId(),  ErrorResponse.class);
        assertThat("The Student does not exist").isEqualTo(Objects.requireNonNull(errorBody.getBody()).getMessage());
    }

    @Test
    public void updateStudent(){
        final String url = LOCALHOST + port + CONTEXT_PATH;
        final String STUDENT_ID = "9";

        ResponseEntity<StudentVO> httpResponse = connectionToApi.connect(url+"/students/"+STUDENT_ID,HttpMethod.GET, StudentVO.class,null);

        StudentVO studentVOById = httpResponse.getBody();

        String firstnameUpdate = "firstname updated";
        String lastnameUpdate = "lastname updated";
        int ageUpdate = 33;

        StudentVO studentVOUpdate = StudentVO.builder()
                .firstname(firstnameUpdate)
                .lastname(lastnameUpdate)
                .age(ageUpdate)
                .average(6.7)
                .build();

        ResponseEntity<StudentVO> httpResponseUpdateStudent = connectionToApi.connect(url+"/students/"+ studentVOById.getId(),HttpMethod.PUT, StudentVO.class, studentVOUpdate);

        StudentVO updatedStudent = httpResponseUpdateStudent.getBody();

        assertTrue(updatedStudent.getId()== studentVOById.getId());
        assertTrue(updatedStudent.getFirstname().equals(firstnameUpdate));
        assertTrue(updatedStudent.getLastname().equals(lastnameUpdate));
        assertTrue(updatedStudent.getAge()==ageUpdate);

        //GET BY ID FOR CHECK THE UPDATE

        ResponseEntity<StudentVO> httpResponseById2 = connectionToApi.connect(url+"/students/"+updatedStudent.getId(),HttpMethod.GET, StudentVO.class,null);

        StudentVO studentVOUpdatedById = httpResponseById2.getBody();

        //CHECK THE DATA
        assertTrue(studentVOUpdate.getAge()== studentVOUpdatedById.getAge());
        assertTrue(studentVOUpdate.getFirstname().equals(studentVOUpdatedById.getFirstname()));
        assertTrue(studentVOUpdate.getLastname().equals(studentVOUpdatedById.getLastname()));
    }

    @Test
    public void deleteANotExistentStudent(){
        final String url = LOCALHOST + port + CONTEXT_PATH;
        boolean isThrowingException = false;
        try {
            connectionToApi.connect(url + "/students/999999", HttpMethod.DELETE, DeleteResponseDTO.class, null);
        }catch(HttpClientErrorException.NotFound ex){
            isThrowingException = true;
        }
        assertTrue(isThrowingException);
    }
}
