package com.softvision.spring_demo.students.dto;

import com.softvision.spring_demo.students.vo.StudentVO;
import com.softvision.spring_demo.students.exception.StudentVOValidationException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static com.softvision.spring_demo.utils.MockFactory.*;

@RunWith(SpringRunner.class)
public class StudentVoTest {

    @Test
    public void createStudentVOSuccessfully() {

        StudentRequest studentRequest = getValidStudentRequest();

        new StudentVO(studentRequest);
    }

    @Test
    public void createdStudentVo_withMinorAverageThatPossible_ThenShouldThrownAnError() {

        StudentRequest studentRequest = getInvalidStudentRequest_minorAverageThatPossible();

        createStudentVOAndVerifyIfThrowsAnError(studentRequest);
    }

    @Test
    public void createdStudentVo_withOverAverageThatPossible_ThenShouldThrownAnError() {

        StudentRequest studentRequest = getInvalidStudentRequest_moreAverageThatPossible();

        createStudentVOAndVerifyIfThrowsAnError(studentRequest);
    }

    @Test
    public void createdStudentVo_withOverAgeThatPossible_ThenShouldThrownAnError() {

        StudentRequest studentRequest = getInvalidStudentRequest_moreAgeThatPossible();

        createStudentVOAndVerifyIfThrowsAnError(studentRequest);
    }

    @Test
    public void createdStudentVo_withMinorAgeThatPossible_ThenShouldThrownAnError() {

        StudentRequest studentRequest = getInvalidStudentRequest_minorAgeThatPossible();

        createStudentVOAndVerifyIfThrowsAnError(studentRequest);
    }

    @Test
    public void createdStudentVo_withNullName_ThenShouldThrownAnError() {

        StudentRequest studentRequest = getInvalidStudentRequest_nullName();

        createStudentVOAndVerifyIfThrowsAnError(studentRequest);
    }

    @Test
    public void createdStudentVo_withNullLastName_ThenShouldThrownAnError() {

        StudentRequest studentRequest = getInvalidStudentRequest_nullLastName();

        createStudentVOAndVerifyIfThrowsAnError(studentRequest);
    }
    ///////////////////
    //Private Methods//
    ///////////////////

    public void createStudentVOAndVerifyIfThrowsAnError(StudentRequest studentRequest) {

        boolean isThrowingAnError = false;

        try {
            new StudentVO(studentRequest);
        } catch (StudentVOValidationException ex) {
            isThrowingAnError = true;
        }
        Assertions.assertTrue(isThrowingAnError);
    }
}
