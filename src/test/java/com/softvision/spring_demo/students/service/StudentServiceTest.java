package com.softvision.spring_demo.students.service;

import com.softvision.spring_demo.students.dto.DeleteResponseDTO;
import com.softvision.spring_demo.students.model.entities.Student;
import com.softvision.spring_demo.students.vo.StudentVO;
import com.softvision.spring_demo.students.exception.ElementNotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.softvision.spring_demo.utils.MockFactory.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class StudentServiceTest {

    @Mock
    StudentService studentService;

    @Test
    public void getStudentById() {

        Student student = getValidStudent();

        when(studentService.getStudentById(anyLong())).thenReturn(student);

        Student studentFromService = studentService.getStudentById(1L);

        Assert.assertTrue(student.getId() == studentFromService.getId());
    }


    @Test
    public void getStudentByWrongId() {
        when(studentService.getStudentById(anyLong())).thenThrow(ElementNotFoundException.class);
        assertThrows(ElementNotFoundException.class,
                () -> {
                    studentService.getStudentById(2938472984723L);
                }
        );
    }

    @Test
    public void saveStudent() {

        Student studentEntity = getValidStudent();

        when(studentService.saveStudent(any(StudentVO.class))).thenReturn(studentEntity);
        studentService.saveStudent(getValidStudentVO());
    }

    @Test
    public void deleteStudent() {

        StudentVO studentVO = getValidStudentVO();

        DeleteResponseDTO deleteResponseDTO = new DeleteResponseDTO(studentVO.getId(), "Deleted");

        when(studentService.deleteStudent(anyLong())).thenReturn(deleteResponseDTO);

        Assert.assertTrue(studentVO.getId() == deleteResponseDTO.getId());

    }

    @Test
    public void deleteNotExistentStudent() {

        when(studentService.deleteStudent(anyLong())).thenThrow(ElementNotFoundException.class);
        assertThrows(ElementNotFoundException.class,
                () -> {
                    studentService.deleteStudent(2938472984723L);
                }
        );
    }

    @Test
    public void getAllStudents() {

        String nameToSearch = "Pedro";
        String lastNameToSearch = "Alvarez";

        List<Student> allStudentsMock = getValidStudents();

        ArrayList<Integer> possibleAges = new ArrayList<>();
        possibleAges.add(32);

        when(studentService.getAllStudents(anyString(), anyString(), anyList())).thenReturn(allStudentsMock);

        List<Student> listFromService = studentService.getAllStudents(nameToSearch, lastNameToSearch, possibleAges);

        Student firstStudent = listFromService.get(0);

        Assert.assertTrue(allStudentsMock.get(0).getFirstname().equals(listFromService.get(0).getFirstname()));
        Assert.assertTrue(firstStudent.getFirstname().equals(nameToSearch));
        Assert.assertTrue(firstStudent.getLastname().equals(lastNameToSearch));
        Assert.assertTrue(allStudentsMock.size() == listFromService.size());
    }
}