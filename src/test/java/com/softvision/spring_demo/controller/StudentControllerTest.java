package com.softvision.spring_demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softvision.spring_demo.students.StudentsApplication;
import com.softvision.spring_demo.students.controller.StudentController;
import com.softvision.spring_demo.students.dto.StudentRequest;
import com.softvision.spring_demo.students.vo.StudentVO;
import com.softvision.spring_demo.students.exception.StudentRequestValidatorException;
import com.softvision.spring_demo.students.helpers.StudentRequestValidator;
import com.softvision.spring_demo.students.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.softvision.spring_demo.utils.MockFactory.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = StudentsApplication.class)
@RunWith(SpringRunner.class)
@WebMvcTest(StudentController.class)
public class StudentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    StudentService studentService;

    @Test
    public void createNewStudent() throws Exception {

        StudentRequest studentRequest = getValidStudentRequest();

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/students")
                .content(new ObjectMapper().writeValueAsString(studentRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        when(studentService.saveStudent(any(StudentVO.class))).thenReturn(getValidStudent());

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        assertThat(HttpStatus.CREATED.value()).isEqualTo(result.getResponse().getStatus());
    }

    @Test
    public void getStudentById() throws Exception {

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/students/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        when(studentService.getStudentById(anyLong())).thenReturn(getValidStudent());

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        assertThat(HttpStatus.OK.value()).isEqualTo(result.getResponse().getStatus());
    }
}